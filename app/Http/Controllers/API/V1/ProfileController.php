<?php

namespace App\Http\Controllers\API\V1;

use App\Models\Profile;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\ValidationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Exception;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $Profile = Profile::all();

            $response = $Profile;
            $code = 200;
        }catch (Exception $e){
            $code = 500;
            $response = $e -> getMessage();
        }

        return apiResponseBuilder($code,$response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required',
            'username' => 'required',
            'address' => 'required',
            'age' => 'required',
            'email' => 'required',
            'phone_number' => 'required'
        ]);

        try {
            $Profile = new Profile();

            $Profile->name = $request->name;
            $Profile->username = $request->username;
            $Profile->address = $request->address;
            $Profile->age = $request->age;
            $Profile->email = $request->email;
            $Profile->phone_number = $request->phone_number;

            $Profile->save();
            $code=200;
            $response=$Profile;

            } catch (Exception $e) {
                if($e instanceof ValidationException){
                    $code = 400;
                    $response = 'tidak ada data';
                }else{
                    $code= 500;
                    $response =$e->getMessage();
                }
            }
            return apiResponseBuilder($code, $response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $Profile = Profile::findOrFail($id);

            $code = 200;
            $response = $Profile;
        }catch (Exception $e){
            if ($e instanceof ModelNotFoundException){
                $code = 400;
                $response = 'inputkan sesuai id';
            }else{
                $code = 500;
                $response = $e->getMessage();
            }
        }

        return apiResponseBuilder($code,$response);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name' => 'required',
            'username' => 'required',
            'address' => 'required',
            'age' => 'required',
            'email' => 'required',
            'phone_number' => 'required'
        ]);

        try {
            $Profile = new Profile();

            $Profile->name = $request->name;
            $Profile->username = $request->username;
            $Profile->address = $request->address;
            $Profile->age = $request->age;
            $Profile->email = $request->email;
            $Profile->phone_number = $request->phone_number;

            $Profile->save();
            $code=200;
            $response=$Profile;

            } catch (Exception $e) {
                if($e instanceof ValidationException){
                    $code = 400;
                    $response = 'tidak ada data';
                }else{
                    $code= 500;
                    $response =$e->getMessage();
                }
            }
            return apiResponseBuilder($code, $response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $Profile = Profile::find($id);
            $Profile->delete();

            $code = 200;
            $response = $Profile;
        }catch (\Exception $e){
            $code = 500;
            $response = $e->getMessage();
        }

        return apiResponseBuilder($code, $response);
    }
}
