<?php

namespace App\Http\Controllers\API\V1;

use App\Models\Project;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\ValidationException;
use Exception;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $Project = Project::all();

            $response = $Project;
            $code = 200;
        }catch (Exception $e){
            $code = 500;
            $response = $e -> getMessage();
        }

        return apiResponseBuilder($code,$response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name_project' => 'required',
            'link_project' => 'required',
            'description_project' => 'required',
            'image' => 'required | image | mimes:jpg,jpeg,png,gif'
        ]);

        try {
            $image = time().'.'.request()->image->getClientOriginalExtension();
            request()->image->move(public_path('image'), $image);

            $Project = new Project();
            $Project->name_project = $request->name_project;
            $Project->link_project = $request->link_project;
            $Project->description_project = $request->description_project;
            $Project->image = $image;

            $Project->save();
            $code=200;
            $response=$Project;

            } catch (Exception $e) {
                if($e instanceof ValidationException){
                    $code = 400;
                    $response = 'tidak ada data';
                }else{
                    $code= 500;
                    $response =$e->getMessage();
                }
            }
            return apiResponseBuilder($code, $response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $Project = Project::findOrFail($id);

            $code = 200;
            $response = $Project;
        }catch (Exception $e){
            if ($e instanceof ModelNotFoundException){
                $code = 400;
                $response = 'inputkan sesuai id';
            }else{
                $code = 500;
                $response = $e->getMessage();
            }
        }

        return apiResponseBuilder($code,$response);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name_project' => 'required',
            'link_project' => 'required',
            'description_project' => 'required',
            'image' => 'required | image | mimes:jpg,jpeg,png,gif'
        ]);

        try {
            $image = time().'.'.request()->image->getClientOriginalExtension();
            request()->image->move(public_path('image'), $image);

            $Project = new Project();
            $Project->name_project = $request->name_project;
            $Project->link_project = $request->link_project;
            $Project->description_project = $request->description_project;
            $Project->image = $image;

            $Project->save();
            $code=200;
            $response=$Project;

            } catch (Exception $e) {
                if($e instanceof ValidationException){
                    $code = 400;
                    $response = 'tidak ada data';
                }else{
                    $code= 500;
                    $response =$e->getMessage();
                }
            }
            return apiResponseBuilder($code, $response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $Project = Project::find($id);
            $Project->delete();

            $code = 200;
            $response = $Project;
        }catch (\Exception $e){
            $code = 500;
            $response = $e->getMessage();
        }

        return apiResponseBuilder($code, $response);
    }
}
